#!/bin/sh
DURATION=3600
SCALE=4000

# collect info about settings
psql pgbench -c "select name, setting from pg_settings order by 1" > settings.log 2>&1

# remove obsolete sar logs
sudo rm -f /var/log/sa/*

# wait for the next sar interval
sleep 60

# test I/O schedulers with default settings
for xlog in cfq noop deadline; do

	for data in cfq noop deadline; do

		d="$xlog-$data"
		mkdir $d

		# warmup is always done with the same schedulers (as a check)
		sudo sh -c "echo cfq > /sys/block/sda/queue/scheduler"
		sudo sh -c "echo cfq > /sys/block/cciss\!c0d1/queue/scheduler"

		# recreate the database drop the database
		dropdb --if-exists pgbench > /dev/null
		createdb pgbench

		# initialize
		pgbench -i -s $SCALE pgbench > $d/init.log 2>&1

		# do freezing and so on
		psql pgbench < vacuum.sql > $d/vacuum.log 2>&1
		vacuumdb -a -F > $d/vacuumdb.log 2>&1

		# warmup (1h)
		pgbench -c 16 -T $DURATION pgbench > $d/warmup.log 2>&1

		# now set the schedulers we're really interested in
		sudo sh -c "echo $xlog > /sys/block/sda/queue/scheduler"
		sudo sh -c "echo $data > /sys/block/cciss\!c0d1/queue/scheduler"

		# make sure the schedulers are the ones requested
		cat /sys/block/sda/queue/scheduler
		cat /sys/block/cciss\!c0d1/queue/scheduler

		# checkpoint and start the benchmark
		psql pgbench -c "checkpoint" > /dev/null 2>&1

		sudo smartctl -a /dev/sda > $d/smartctl.sda.before.log 2>&1

		for i in `seq 0 6`; do
			sudo smartctl -a /dev/cciss/c0d1 -d cciss,$i > $d/smartctl.c0d1.$i.before.log 2>&1
		done

		s=`date +%s`

		pgbench -c 16 -T $DURATION -l --aggregate-interval=1 pgbench > $d/pgbench.log 2>&1

		e=`date +%s`

		sudo smartctl -a /dev/sda > $d/smartctl.sda.after.log 2>&1

                for i in `seq 0 6`; do
			sudo smartctl -a /dev/cciss/c0d1 -d cciss,$i > $d/smartctl.c0d1.$i.after.log 2>&1
                done

		mv pgbench_log.* $d/pgbench.translog

		sh collect-sar.sh $s $e $d

	done;

done;

# also, test various a bit more aggressive deadline settings (defaults are 500 / 5000)
# we'll set noop for sda (which is where pg_xlog resides) and deadline for sdb (pgdata)

for rexpire in 50 100 250; do

	for wexpire in 50 100 250 500; do

		d="noop-deadline-$rexpire-$wexpire"
		mkdir $d

		# warmup is always done with the same schedulers (as a check)
		sudo sh -c "echo cfq > /sys/block/sda/queue/scheduler"
		sudo sh -c "echo cfq > /sys/block/cciss\!c0d1/queue/scheduler"

		# recreate the database drop the database
		dropdb --if-exists pgbench > /dev/null
		createdb pgbench

		# initialize
		pgbench -i -s $SCALE pgbench > $d/init.log 2>&1

		# do freezing and so on
		psql pgbench < vacuum.sql > $d/vacuum.log 2>&1
		vacuumdb -a -F > $d/vacuumdb.log 2>&1

		# warmup (1h)
		pgbench -c 16 -T $DURATION pgbench > $d/warmup.log 2>&1

		# now set the noop/deadline schedulers we're intested in
		sudo sh -c "echo noop > /sys/block/sda/queue/scheduler"
		sudo sh -c "echo deadline > /sys/block/cciss\!c0d1/queue/scheduler"

		# make sure the schedulers are the ones requested
		cat /sys/block/sda/queue/scheduler
		cat /sys/block/cciss\!c0d1/queue/scheduler

		# also set the parameters for deadline
		sudo sh -c "echo $rexpire > /sys/block/sdb/queue/iosched/read_expire"
		sudo sh -c "echo $wexpire > /sys/block/sdb/queue/iosched/write_expire"

		psql pgbench -c "checkpoint" > /dev/null 2>&1

		sudo smartctl -a /dev/sda > $d/smartctl.sda.before.log 2>&1

		for i in `seq 0 6`; do
			sudo smartctl -a /dev/cciss/c0d1 -d cciss,$i > $d/smartctl.c0d1.$i.before.log 2>&1
		done

		s=`date +%s`

		pgbench -c 16 -T $DURATION -l --aggregate-interval=1 pgbench > $d/pgbench.log 2>&1

		e=`date +%s`

		sudo smartctl -a /dev/sda > $d/smartctl.sda.after.log 2>&1

		for i in `seq 0 6`; do
			sudo smartctl -a /dev/cciss/c0d1 -d cciss,$i > $d/smartctl.c0d1.$i.after.log 2>&1
		done

		mv pgbench_log.* $d/pgbench.translog

		sh collect-sar.sh $s $e $d

	done
done
