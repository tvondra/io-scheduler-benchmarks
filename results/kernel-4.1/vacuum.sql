SET vacuum_freeze_table_age = 0;
SET vacuum_freeze_min_age = 0;
SET vacuum_multixact_freeze_table_age = 0;
SET vacuum_multixact_freeze_min_age = 0;

VACUUM FREEZE;
