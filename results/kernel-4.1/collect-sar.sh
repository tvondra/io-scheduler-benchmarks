#!/bin/sh

stimestamp=$1
etimestamp=$2
outdir=$3

sdate=`date -d "@$stimestamp"`
edate=`date -d "@$etimestamp"`

sday=`date -d "$sdate" +%d`
eday=`date -d "$edate" +%d`

while true; do

        # two-char format
        # d=$(printf "%02d" $sday)
	d=$sday

        # paging statistics
        sadf -d -- -B /mnt/data/sysstat/sa$d >> sar.paging.data

        # i/o stats
        sadf -d -- -b /mnt/data/sysstat/sa$d >> sar.io.data

        # block devices
        sadf -d -- -d -p /mnt/data/sysstat/sa$d >> sar.devices.data

        # network stats
        sadf -d -- -n ALL /mnt/data/sysstat/sa$d >> sar.network.data

        # per-cpu stats
        sadf -d -- -P ALL /mnt/data/sysstat/sa$d >> sar.cpudetails.data

        # cpu utilization
        sadf -d -- -u ALL /mnt/data/sysstat/sa$d >> sar.cpu.data

        # queueing stats
        sadf -d -- -q  /mnt/data/sysstat/sa$d >> sar.queues.data

        # memory statistics
        sadf -d -- -R /mnt/data/sysstat/sa$d >> sar.memstats.data

        # memory utilization
        sadf -d -- -r /mnt/data/sysstat/sa$d >> sar.memutil.data

        # swap stats
        sadf -d -- -S /mnt/data/sysstat/sa$d >> sar.swap.data

        # swapping stats
        sadf -d -- -W /mnt/data/sysstat/sa$d >> sar.swapstats.data

        # task creation / context switching
        sadf -d -- -w /mnt/data/sysstat/sa$d >> sar.tasks.data

        # stop the loop if this was the last day
        if [ "$sday" -eq "$eday" ]; then
               	break;
        fi

        sdate=`date -d "$sdate + 1 day"`
        sday=`date -d "$sdate" +%d`

done

tar -czf $outdir/sar.tgz sar.* > /dev/null 2>&1
rm sar.*

# remove unnecessary sar files
sudo rm -f /mnt/data/sysstat/*

# make sure the next sar interval starts
sleep 60
