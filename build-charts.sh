#!/bin/sh

cdir=`pwd`
odir="$cdir/charts"

rm -Rf charts
mkdir charts

for i in 1 2 3; do

	cd $cdir/results/$i
	mkdir $odir/$i
	x=2

	echo "pct" > $odir/$i/tps.csv
	echo "pct" > $odir/$i/minlat.csv
	echo "pct" > $odir/$i/maxlat.csv

	for p in `seq 0 100`; do
		echo $p >> $odir/$i/tps.csv
		echo $p >> $odir/$i/minlat.csv
		echo $p >> $odir/$i/maxlat.csv
	done

	echo "set terminal postscript eps enhanced color font 'Helvetica,10' size 4,2" > $odir/$i/tps.plot
	echo "set output 'charts/$i/tps.eps'" >> $odir/$i/tps.plot
	echo "set key top left" >> $odir/$i/tps.plot
	echo "set title 'pgbench tps CDF' font 'Helvetica, 15'" >> $odir/$i/tps.plot
	echo "set xlabel 'transactions per second'" >> $odir/$i/tps.plot
	echo "set ylabel 'probability'" >> $odir/$i/tps.plot
	echo "set label 'scale 4000, 16 clients' at screen 0.75, 0.95 font 'Helvetica, 10'" >> $odir/$i/tps.plot
	echo "set grid ytics lt 0 lw 1 lc rgb '#bbbbbb'" >> $odir/$i/tps.plot
	echo" set grid xtics lt 0 lw 1 lc rgb '#bbbbbb'" >> $odir/$i/tps.plot
	echo "plot \\" >> $odir/$i/tps.plot

	echo "set terminal postscript eps enhanced color font 'Helvetica,10' size 4,2" > $odir/$i/minlat.plot
	echo "set output 'charts/$i/minlat.eps'" >> $odir/$i/minlat.plot
	echo "set key top left" >> $odir/$i/minlat.plot
	echo "set title 'minimum latency CDF' font 'Helvetica, 15'" >> $odir/$i/minlat.plot
	echo "set xlabel 'latency [microseconds]'" >> $odir/$i/minlat.plot
	echo "set ylabel 'probability'" >> $odir/$i/minlat.plot
	echo "set label 'scale 4000, 16 clients' at screen 0.75, 0.95 font 'Helvetica, 10'" >> $odir/$i/minlat.plot
	echo "set grid ytics lt 0 lw 1 lc rgb '#bbbbbb'" >> $odir/$i/minlat.plot
	echo" set grid xtics lt 0 lw 1 lc rgb '#bbbbbb'" >> $odir/$i/minlat.plot
	echo "plot \\" >> $odir/$i/minlat.plot

	echo "set terminal postscript eps enhanced color font 'Helvetica,10' size 4,2" > $odir/$i/maxlat.plot
	echo "set output 'charts/$i/maxlat.eps'" >> $odir/$i/maxlat.plot
	echo "set key top left" >> $odir/$i/maxlat.plot
	echo "set title 'maximum latency CDF' font 'Helvetica, 15'" >> $odir/$i/maxlat.plot
	echo "set xlabel 'latency [microseconds]'" >> $odir/$i/maxlat.plot
	echo "set ylabel 'probability'" >> $odir/$i/maxlat.plot
	echo "set label 'scale 4000, 16 clients' at screen 0.75, 0.95 font 'Helvetica, 10'" >> $odir/$i/maxlat.plot
	echo "set grid ytics lt 0 lw 1 lc rgb '#bbbbbb'" >> $odir/$i/maxlat.plot
	echo" set grid xtics lt 0 lw 1 lc rgb '#bbbbbb'" >> $odir/$i/maxlat.plot
	echo "plot \\" >> $odir/$i/maxlat.plot

	for d in `find ./ -type d | sort`; do

		d=${d/.\//}

		if [[ "$d" != "" ]]; then

			tps=`grep excluding $d/pgbench.log | sed 's/.*tps = \(.*\)\..*/\1/'`
			echo "$i	$d	$tps"

			# tps CDF

			cat $d/pgbench.translog | awk '{print $2}' | sort -n > /tmp/trans.log
			cnt=`wc -l /tmp/trans.log | awk '{print $1}'`

			pct=$((cnt/100))

			echo "$d" > $odir/$i/$d.tps.csv
			for p in `seq 0 100`; do

				n=$(($p*$pct+1))
				tps=`head -n $n /tmp/trans.log | tail -n 1`

				echo "$tps" >> $odir/$i/$d.tps.csv

			done

			# min latency CDF

			cat $d/pgbench.translog | awk '{print $5}' | sort -n > /tmp/trans.log
			cnt=`wc -l /tmp/trans.log | awk '{print $1}'`

			pct=$((cnt/100))

			echo "$d" > $odir/$i/$d.minlat.csv
			for p in `seq 0 100`; do

				n=$(($p*$pct+1))
				minlat=`head -n $n /tmp/trans.log | tail -n 1`

				echo "$minlat" >> $odir/$i/$d.minlat.csv

			done

			# max latency CDF

			cat $d/pgbench.translog | awk '{print $6}' | sort -n > /tmp/trans.log
			cnt=`wc -l /tmp/trans.log | awk '{print $1}'`

			pct=$((cnt/100))

			echo "$d" > $odir/$i/$d.maxlat.csv
			for p in `seq 0 100`; do

				n=$(($p*$pct+1))
				maxlat=`head -n $n /tmp/trans.log | tail -n 1`

				echo "$maxlat" >> $odir/$i/$d.maxlat.csv

			done

			paste $odir/$i/tps.csv $odir/$i/$d.tps.csv > tmp
			mv tmp $odir/$i/tps.csv

			paste $odir/$i/minlat.csv $odir/$i/$d.minlat.csv > tmp
			mv tmp $odir/$i/minlat.csv

			paste $odir/$i/maxlat.csv $odir/$i/$d.maxlat.csv > tmp
			mv tmp $odir/$i/maxlat.csv

			echo "'charts/$i/tps.csv' using $x:1 with lines title '$d', \\" >> $odir/$i/tps.plot
			echo "'charts/$i/minlat.csv' using $x:1 with lines title '$d', \\" >> $odir/$i/minlat.plot
			echo "'charts/$i/maxlat.csv' using $x:1 with lines title '$d', \\" >> $odir/$i/maxlat.plot

			x=$((x+1))

		fi

	done;

	cd $cdir

	gnuplot $odir/$i/tps.plot
	gnuplot $odir/$i/minlat.plot
	gnuplot $odir/$i/maxlat.plot

done
